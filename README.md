Setup Instructions 
 
 Prerequisites
 The setups steps expect following versions installed on the system.

 Ruby 2.5.3
 Rails 5.2.3

 1) Extract the code
 2) Move into project folder and run "bundle install"
 3) Run server by using command "rails server"
 4) You can access application on http://localhost:3000
 
 
 Routes
 
 1) For taking off drone
 `get '/takeoff'`
 
 2) For moving drone left 
 `get '/moveleft'`
 
 3) For moving drone up
 `get '/moveup'`
 
 4) For moving drone right
 `get '/moveright'`
 
 5) For moving drone forward
 `get '/moveforward'`
 
 6) For moving drone back
 `get '/moveback'`
 
 7) For moving drone down
 `get '/movedown'`
 
 8) For stabilizing the drone
 `get '/stabilize'`
 
 9) For landing the drone
 `get '/land'`
 
 10) For getting drone current status
 `get '/status'`
 
 11) If the drone is broken
 `get '/broke'`
 
 12) If we tap the drone
 `get '/tap'`
 
 Note: All routes are using GET method and will respond drone status in JSON format