Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/takeoff', to: 'drone#take_off'

  get '/moveleft', to: 'drone#move_left'

  get '/moveup', to: 'drone#move_up'

  get '/moveright', to: 'drone#move_right'

  get '/moveforward', to: 'drone#move_forward'

  get '/moveback', to: 'drone#move_back'

  get '/movedown', to: 'drone#move_down'

  get '/stabilize', to: 'drone#stabilize'

  get '/land', to: 'drone#land'

  get '/status', to: 'drone#status'

  get '/broke', to: 'drone#broke'

  get '/tap', to: 'drone#tap'
end
