class DroneController < ApplicationController
  before_action :engine_status

    # engine will have status on and off
    # 0 = off
    # 1 = on
    @@engine_status = 0

    # drone status would be off, hovering, moving
    @@drone_status = "off"

    # co-ordinates of drone
    @@x_coordinate = 0
    @@y_coordinate = 0
    @@z_coordinate = 0

    @@power_indicator = 100

  # GET /takeoff function to take off drone
  def take_off
    if @status == 0
      @@engine_status = 1
      @@drone_status = "moving"
      @@z_coordinate += 10
      @@drone_status = "hovering"
    else
      logger.debug "Take off failure"
    end
    get_status
  end

  # GET /moveleft function to move drone left
  def move_left
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@x_coordinate -=5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /moveright function to move drone right
  def move_right
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@x_coordinate +=5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /moveforward function to move drone forward
  def move_forward
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@y_coordinate += 5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /moveback function to move drone backward
  def move_back
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@y_coordinate -=5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /moveup function to move drone up
  def move_up
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@z_coordinate +=5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /movedown function to move drone down
  def move_down
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "moving"
      @@z_coordinate -=5
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /stabilize function to stabilize dron
  def stabilize
    if @status == 1 && @@drone_status != "off"
      @@drone_status = "hovering"
    end
    get_status
  end

  # GET /status function to get drone status
  def status
    get_status
  end

  # GET /land function to landing drone
  def land
    if @status == 1 && @@drone_status != "off"
      @@x_coordinate = 0
      @@y_coordinate = 0
      @@z_coordinate = 0
      @@drone_status = "off"
    end
    get_status
  end

  # GET /broke function will run when drone breaks
  def broke
    if @status == 1 && @@drone_status != "off"
      if @@power_indicator <= 10
        logger.debug "Drone Broke"
        land
      end
    end
    get_status
  end

  # GET /tap function will stabilize drone when we tap it
  def tap
    logger.debug "Stabilizing Drone"
    stabilize
  end

  private
  # function to check engine status
  def engine_status
    @status = @@engine_status
  end

  # function to get current status of drone
  def get_status
    @@data = []
    @@data.push({
                    'engine_status': @@engine_status,
                    'drone_status': @@drone_status,
                    'x': @@x_coordinate,
                    'y': @@y_coordinate,
                    'z': @@z_coordinate,
                })

    render json: @@data
  end
end